console.log("Mabuhay");


// JS - is a loosely type Programming Language

alert("Hello Again!");

console. log ( "Hello World!" ) ;

console.
log
(
	"Hello World!"
);

// [SECTION] - Making Comments in JS

/*

	1. Single-Line comment - ctrl + /
	2. Multi-Line comment - ctrl + shift + /

*/

// [SECTION] - Variables
// used to contain data
// usually stored in a computer's memory.


// Syntax -- let variableName;

let myVariables;

console.log(myVariables);

// Syntax -- let variableName variableValue;
let mySecondVariables = 2;


console.log(mySecondVariables);


let productName = "desktop computer";
console.log(productName);

// Reassigning Value

let friend;

friend = "Kate";
friend = "Jane";
friend = "Loyd";
console.log(friend);

// Syntax const variableName variableValue

const pet = "Bruno";
// pet = "Lala";
console.log(pet);

// const hoursPerDay = 24;

// Local and Global variables

let outerVariable = "Hello"; //This is a global variable

{
	let innerVariable = "Hello World!" // Local Variable
	console.log(innerVariable);

}

console.log(outerVariable);

const outerExample = "Global Variable";

{
	const innerExample = "Local Variable";
	console.log(innerExample);
}

console.log(outerExample);


// Multiple Declaration

let productCode = "DC017", productBrand = "Dell";

/*let productCode = "DC017";
let productBrand = "Dell";*/

console.log	(productCode, productBrand);

// [SECTION] Data Type

// Strings
// Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
// Strings in Javascript can be written using either a single(') or double(") quote

let country = 'Philippines';
let province = "Metro Manila";

// Concatenating Strings
// Multiple string values can be combined to create a single string using "+" symbol

let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in" + ", " + country;
console.log(greeting);


let mailAddress = "Metro Manila\n\nPhilippines"
console.log(mailAddress);

let message = "John's employees went home early";
message = 'John\'s employees went home early';
console.log(message);

// Numbers
let headcount = 26;
console.log(headcount);

// Decimal numbers or fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10
console.log(planetDistance);

// Combining string and intergers/numbers

console.log("My grade last sem is " + grade);

// Boolean
// Boolean values are normally used to store values relating to the state of certain things

let isMarried = false;
let inGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays
// Arrays are a special kind of data type that's used to store multiple values
// Arrays can store different data types but is normally used to store similar data types
// let/const arrayName = [elementA, elementB, elementC, .....]

let grades = [98.7, 92.1, 90.2, 94.6]
console.log(grades);

// Array with different Data Types
let details = ["John", "Smith", 32 , true];
console.log(details);

// Objects
// Objects are another special kind of data type that's used to mimic real world objects/items
// Syntax
  // let/const objectName = {
  // 	propertyA: value,
  // 	propertyB: value,
  // }

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["09123456789", "0999999999"],
	address: { //nester object
		houseNumber: "345",
		city: "Manila"
	}
}

console.log(person);

// re-assigning value to an array
const anime = ["one piece", "one punch man", "Attack on Titan"];
anime[1] = "Kimetsu no Yaiba";

console.log(anime);